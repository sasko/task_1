﻿#include <iostream>
#include <fstream>
#include <algorithm>

void logger(std::ostream&cerr); //передаём поток ввода
size_t getArrayLength(std::ifstream* inputFile);// передаём файл, возвращаем длину будущего массива
int* createArray(const size_t length);// передаём длину,создаём динамический массив, возращаем массив
bool getArrayFromFile(std::ifstream* inputFile, int* array); // проверяем записало ли числа с файла в массив
void sort(int* array, const size_t length);//сортировка
void endl(std::ostream& cout);// отступ
void fill(int* arr, std::ifstream* inputFile, const size_t length); // считывание 1 и 2 массивов
void add_arrays(int* arr_1, int* arr_2, int* arr_3, const size_t length_arr_1, const size_t length_arr_3); // сливаем два массива в третий

int main()
{
    std::ifstream inputNumArrayFile;
    std::string nameFile_1{ "numbersOfArray_1.txt" };
    inputNumArrayFile.open(nameFile_1);
    if (!inputNumArrayFile.is_open())
    {
        logger(std::cerr);
        return 0;
    }
    size_t length_array_1 = getArrayLength(&inputNumArrayFile);
    inputNumArrayFile.close();

    inputNumArrayFile.open(nameFile_1);
    if (!inputNumArrayFile.is_open())
    {
        logger(std::cerr);
        return 0;
    }
    int* arr_1 = createArray(length_array_1);
    fill(arr_1, &inputNumArrayFile, length_array_1);
   // const bool fileProcessed = getArrayFromFile(&inputNumArrayFile, arr_1);
   // std::cout << fileProcessed;
    inputNumArrayFile.close();

    endl(std::cout);

    std::string nameFile_2{ "numbersOfArray_2.txt" };
    inputNumArrayFile.open(nameFile_2);
    if (!inputNumArrayFile.is_open())
    {
        logger(std::cerr);
        return 0;
    }
    size_t length_array_2 = getArrayLength(&inputNumArrayFile);
    inputNumArrayFile.close();

    inputNumArrayFile.open(nameFile_2);
    if (!inputNumArrayFile.is_open())
    {
        logger(std::cerr);
        return 0;
    }
    int* arr_2 = createArray(length_array_2);
    fill(arr_2, &inputNumArrayFile, length_array_2);
    //const bool fileProcessed = getArrayFromFile(&inputNumArrayFile,arr_2);
    inputNumArrayFile.close();

    endl(std::cout);

    size_t length_array_3 = length_array_1 + length_array_2;
    int* arr_3 = createArray(length_array_3);
    add_arrays(arr_1, arr_2, arr_3, length_array_1, length_array_3);

    endl(std::cout);

    delete[] arr_1;
    arr_1 = nullptr;
    delete[] arr_2;
    arr_2 = nullptr;
    sort(arr_3, length_array_3);
    delete[] arr_3;
    arr_3 = nullptr;

    return 0;
}

void add_arrays(int* arr_1, int* arr_2, int* arr_3 , const size_t length_arr_1, const size_t length_arr_3)
{
    for (size_t i = 0; i < length_arr_3; ++i) // сливаем первых два массива в третий
    {
        arr_3[i] = i < length_arr_1 ? arr_1[i] : arr_2[i - length_arr_1];
        std::cout << arr_3[i] << " ";
    }
}

void fill(int* arr, std::ifstream* inputFile, const size_t length)
{
    for (size_t i = 0; i < length; ++i)
    {
        *inputFile >> arr[i];
        std::cout << arr[i] << " ";
    }
}

void endl(std::ostream& cout)
{
    std::cout << std::endl;
}

void logger(std::ostream&cerr)
{
    std::cerr << "Error" << std::endl;
}

size_t getArrayLength(std::ifstream* inputFile)
{
    int a;
    size_t length = 0;
    while (*inputFile >> a)
        ++length;
    return length;
}

int* createArray(const size_t length)
{
    int* array = new (std::nothrow) int[length];
    if (array == nullptr)
    {
        logger(std::cerr);
        return 0;
    }
    else
    {
        return array;
    }
}

/*bool getArrayFromFile(std::ifstream* inputFile,int* array)
{
    int a;
    size_t i = 0;
    while (*inputFile >> a)
    {
        i++;
        if (a == array[i])
            return true;
        else
            return false;
    }
       
}*/

void sort(int* array, const size_t length)
{
    for (size_t startIndex = 0; startIndex < length - 1; ++startIndex)
    {
        size_t smallestIndex = startIndex;

        for (size_t currentIndex = startIndex + 1; currentIndex < length; ++currentIndex)
        {

            if (array[currentIndex] < array[smallestIndex])
            {
                smallestIndex = currentIndex;
            }
        }
        std::swap(array[startIndex], array[smallestIndex]);
    }
    for (size_t i = 0; i < length; ++i)
    {
        std::cout << array[i] << " ";
    }
}