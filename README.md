# Description problem solving
-----------------------------

## Problems
-----------
* check for memory allocation for arrays
* sorting
* delete memory in the end of using dynamics arrays

## Solving
----------
**1.**
```
if (!name) // проверка на выделение памяти для первого динамического массива
	{
		cerr << "Could not allocate memory" << endl;
		return 0;
	}
```
\[ **name** - name of array.\]

**2.** I used selection sort

**3.** 
```
delete[] name;
name = nullptr;
```
\[ **name** - name of array.\]

## Сonclusion
-------------
I have had many problems but I tried to solve them